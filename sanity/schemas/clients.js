import { MdPerson as icon } from 'react-icons/md';

export default {
  // computer name
  name: 'clients',
  // visible title
  // icon: () => `🥩`,
  icon,
  title: 'Clients',
  type: 'document',
  fields: [
    {
      name: 'name',
      title: 'title',
      type: 'string',
      description: 'Name of the client',
    },
    {
      name: 'url',
      title: 'URL',
      type: 'string',
    },
    {
      name: 'description',
      title: 'Description',
      type: 'text',
    },
    {
      name: 'image',
      title: 'Image',
      type: 'image',
      options: {
        hotspot: true,
      },
    },
  ],
};
