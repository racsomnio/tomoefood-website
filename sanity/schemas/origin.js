import { FaTags as icon } from 'react-icons/fa';

export default {
  // computer name
  name: 'origin',
  // icon: () => `🥩`,
  icon,
  // visible title
  title: 'Tags',
  type: 'document',
  fields: [
    {
      name: 'name',
      title: 'title',
      type: 'string',
      description: 'This is a tag that will filter the options',
    },
    {
      name: 'order',
      title: 'Order On Line',
      description: 'Can I buy it in OrderWagyu.com?',
      type: 'boolean',
      options: {
        layout: 'switch',
      },
    },
    {
      name: 'category',
      title: 'Category',
      description: 'Is this a category?',
      type: 'boolean',
      options: {
        layout: 'switch',
      },
    },
  ],
  preview: {
    select: {
      name: 'name',
      order: 'order',
    },
    prepare: ({ name, order }) => ({
      title: `${name} ${order ? '🛍️' : ''}`,
    }),
  },
};
