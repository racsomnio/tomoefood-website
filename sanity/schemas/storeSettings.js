import { MdStore as icon } from 'react-icons/md';

export default {
  // computer name
  name: 'storeSettings',
  // visible title
  // icon: () => `🥩`,
  icon,
  title: 'Settings',
  type: 'document',
  fields: [
    {
      name: 'name',
      title: 'Store Title',
      type: 'string',
      description: 'Name of product',
    },
    {
      name: 'clients',
      title: 'Current clients',
      type: 'array',
      of: [{ type: 'reference', to: [{ type: 'clients' }] }],
    },
    {
      name: 'restaurants',
      title: 'Current Restaurants',
      type: 'array',
      of: [{ type: 'reference', to: [{ type: 'restaurants' }] }],
    },
    {
      name: 'popularProducts',
      title: 'Popular Products',
      type: 'array',
      of: [{ type: 'reference', to: [{ type: 'products' }] }],
    },
  ],
};
