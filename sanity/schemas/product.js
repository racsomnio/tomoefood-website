import { GiCow as icon } from 'react-icons/gi';
import PriceInput from '../components/PriceInput';

export default {
  // computer name
  name: 'products',
  // visible title
  // icon: () => `🥩`,
  icon,
  title: 'Products',
  type: 'document',
  fields: [
    {
      name: 'name',
      title: 'title',
      type: 'string',
      description: 'Name of product',
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: 'name',
        maxLength: 100,
      },
    },
    {
      name: 'image',
      title: 'Image',
      type: 'image',
      options: {
        hotspot: true,
      },
    },
    {
      name: 'price',
      title: 'Price',
      type: 'number',
      description: 'Price is in cents',
      validation: (Rule) => Rule.min(1000),
      inputComponent: PriceInput,
    },
    {
      name: 'url',
      title: 'External URL',
      type: 'url',
      description: 'Insert URL to be linked to',
    },
    {
      name: 'origin',
      title: 'Origin',
      type: 'array',
      of: [{ type: 'reference', to: [{ type: 'origin' }] }],
    },
  ],
  preview: {
    select: {
      title: 'name',
      media: 'image',
      origin0: 'origin.0.name',
      origin1: 'origin.1.name',
    },
    prepare: ({ title, media, ...tags }) => {
      const isTag = Object.values(tags).filter(Boolean);
      return {
        title,
        media,
        subtitle: Object.values(isTag).join(', '),
      };
    },
  },
};
