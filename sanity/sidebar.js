import React from 'react';
import S from '@sanity/desk-tool/structure-builder';

export default function Sidebar() {
  return S.list()
    .title('Tomoe Food Services')
    .items([
      S.listItem()
        .title('Home Page')
        .icon(() => <strong>🔥</strong>)
        // customize documentId
        .child(S.editor().schemaType('storeSettings').documentId('stock')),
      // add in the rest of docmuments and hide the storeSettings from sidebar
      ...S.documentTypeListItems().filter(
        (item) => item.getId() !== 'storeSettings'
      ),
    ]);
}
