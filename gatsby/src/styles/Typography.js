import { createGlobalStyle } from 'styled-components';

import font from '../assets/fonts/Aileron-Thin.otf';

const Typography = createGlobalStyle`
  @font-face {
    font-family: 'aileronthin';
    src: url(${font});
    display: swap;
  }
  html {
    font-family: 'aileronthin', BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,Helvetica,Arial,sans-serif;
    -webkit-font-smoothing: antialiased;
    color: var(--black);
  }
  p, li {
    letter-spacing: 0.5px;
  }
  h1,h2,h3,h4,h5,h6 {
    font-weight: normal;
    margin: 0;
  }
  a {
    color: var(--secondary);
    text-decoration: none;
    /* Chrome renders this weird with this font, so we turn it off */
    text-decoration-skip-ink: none;
  }
  mark, .mark {
    padding: 0 10px;
    margin: 2rem 0 0;
    display: inline;
    line-height: 1;
    text-align: center;
  }

  .center {
    text-align: center;
  }
`;

export default Typography;
