import { createGlobalStyle } from 'styled-components';
import bg from '../assets/images/j-pattern.jpg';

const GlobalStyles = createGlobalStyle`
  :root {
    --red: #c21b27;
    --black: #2E2E2E;
    --yellow: #ffc600;
    --white: #fff;
    --grey: #efefef;
    --primary: #000000;
    --secondary: #b8aa80;
    --text: #fff;
    --nav: -250px;
  }
  html {
    /* background-image: url(${bg}); */
    /* background-size: 450px; */
    /* background-attachment: fixed; */
    font-size: 10px;
  }

  body {
    font-size: 2rem;
    background: #000;
    color: #fff;
    width: 100%;
    transition: margin .5s;

    &.nav-open {
      margin-left: -250px;
      @media (min-width: 780px) {
        margin-left: 0;
      }
    }
  }

  .content {
    max-width: 800px;
    margin: auto;
  }

  .primary-bg {
    display: inline-block;
    color: var(--secondary);
    font-size: 6rem;
    margin-bottom: 3rem;
    padding: 2rem;
  }

  .secondary-bg {
    display: inline-block;
    color: #fff;;
    font-size: 6rem;
    margin-bottom: 1.5rem;
    padding: 2rem;
  }

  fieldset {
    border-color: rgba(255,255, 255,0.2);
    border-width: 1px;
  }

  button, .button {
    background: var(--secondary);
    color: #fff;;
    border: 0;
    padding: 1.2rem 3rem;
    cursor: pointer;
    text-shadow: 0.5px 0.5px 2px rgba(0,0,0,0.5);
    transition: all 0.2s;
    &:hover {
      box-shadow: 0 0 1px 1px rgba(0,0,0,0.2);
    }
  }

  .gatsby-image-wrapper img[src*='base64\\,'] {
    image-rendering: -moz-crisp-edges;
    image-rendering: crisp-edges;
  }

  /* Scrollbar Styles */
  /* body::-webkit-scrollbar {
    width: 12px;
  }
  html {
    scrollbar-width: thin;
    scrollbar-color: var(--red) var(--white);
  }
  body::-webkit-scrollbar-track {
    background: var(--white);
  }
  body::-webkit-scrollbar-thumb {
    background-color: var(--red) ;
    border-radius: 6px;
    border: 3px solid var(--white);
  } */

  hr {
    border: 0;
    height: 8px;
  }

  img {
    max-width: 100%;
  }

  .headroom {
    top: 0;
    left: 0;
    width: 100vw;
    z-index: 1;
    transition: left 0.5s;
  }
  .headroom--unfixed {
    position: relative;
    transform: translateY(0%);
    transition: all 0.5s;
  }
  .headroom--scrolled {
    transition: transform 200ms ease-in-out;
  }
  .headroom--unpinned {
    position: fixed;
    transform: translateY(0%);
  }
  .headroom--pinned {
    position: fixed;
    transform: translateY(0%);
  }

  .headroom--unpinned, .headroom--pinned {
    .nav-open & {
      left: var(--nav);
      @media (min-width: 780px) {
        left: 0;
      }
    }
  }
`;

export default GlobalStyles;
