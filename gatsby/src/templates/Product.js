import React from 'react';
import { graphql } from 'gatsby';
import Img from 'gatsby-image';
import styled from 'styled-components';
import SEO from '../components/SEO';

const ProductGrid = styled.div`
  display: grid;
  grid-gap: 2rem;
  grid-template-columns: repeat(auto-fill, minmax(400px, 1fr));
`;

export default function SinglePizzaPage({ data: { product } }) {
  return (
    <>
      <SEO title={product.name} image={product.image?.asset?.fluid?.src} />
      <ProductGrid>
        <Img fluid={product.image.asset.fluid} />
        <div>
          <h2 className="mark">{product.name}</h2>
          <ul>
            {product.origin.map((tag) => (
              <li key={tag.id}>{tag.name}</li>
            ))}
          </ul>
        </div>
      </ProductGrid>
    </>
  );
}

// This needs to be dynamic based on the slug passed in via context in gatsby-node.js
export const query = graphql`
  query($slug: String!) {
    product: sanityProducts(slug: { current: { eq: $slug } }) {
      name
      id
      image {
        asset {
          fluid(maxWidth: 800) {
            ...GatsbySanityImageFluid
          }
        }
      }
      origin {
        name
        id
        category
        order
      }
    }
  }
`;
