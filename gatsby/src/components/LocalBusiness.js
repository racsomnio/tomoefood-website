import React from 'react';
import styled from 'styled-components';

const BusinessStyles = styled.div`
  h2 {
    font-size: inherit;
  }
`;

export default function LocalBusiness() {
  return (
    <BusinessStyles>
      <div>
        <span>355 Food Center Drive, Building E 15</span>
        <span>The Bronx</span>,<span>NY</span>
      </div>
      Phone: <span>718-328-1129</span>
    </BusinessStyles>
  );
}
