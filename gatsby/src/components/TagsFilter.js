import React from 'react';
import { useStaticQuery, graphql, Link } from 'gatsby';
import styled from 'styled-components';

const TagsStyles = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 1rem;
  margin-bottom: 4rem;
  padding: 2rem;
  
  a {
    display: grid;
    grid-template-columns: auto 1fr;
    padding: 5px;
    grid-gap: 0 1rem;
    align-items: center;
    background: var(--grey);
    border-radius: 10px;
    .count {
      background: #fff;
      padding: 2px 10px;
      border-radius: 20px;
    }
    &[aria-current='page'] {
      background: var(--yellow);
    }
  }
`;

function existingTags(products) {
  const counts = products
    .map((product) => product.origin)
    .flat()
    .reduce((a, origin) => {
      const existingOrigin = a[origin.id];
      if (existingOrigin) {
        existingOrigin.count += 1;
      } else {
        a[origin.id] = {
          id: origin.id,
          name: origin.name,
          count: 1,
        };
      }
      return a;
    }, {});

  const sortedOrigins = Object.values(counts).sort((a, b) => b.count - a.count);

  return sortedOrigins;
}

export default function TagsFilter() {
  // *** dynamic queries cane be done only from page, useQuery inside components *****
  const { tags, products } = useStaticQuery(graphql`
    query {
      tags: allSanityOrigin {
        nodes {
          name
          id
          category
          order
        }
      }
      products: allSanityProducts {
        nodes {
          origin {
            id
            name
          }
        }
      }
    }
  `);

  const productsWithTags = existingTags(products.nodes);

  return (
    <TagsStyles>
      <Link to="/products">
        <span className="name">All</span>
        <span className="count">{products.nodes.length}</span>
      </Link>
      {productsWithTags.map((tag) => (
        <Link key={tag.id} to={`/products/${tag.name}`}>
          <span className="name">{tag.name}</span>
          <span className="count">{tag.count}</span>
        </Link>
      ))}
    </TagsStyles>
  );
}
