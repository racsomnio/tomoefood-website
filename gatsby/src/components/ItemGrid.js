import React from 'react';
import { ItemsGrid, ItemStyles } from '../styles/Grids';

export default function ItemGrid({ items }) {
  return (
    <ItemsGrid>
      {items.map((item) => (
        <a href={item.url} target="_blank" rel="noreferrer" key={item._id}>
          <ItemStyles>
            <img
              width="500"
              height="400"
              src={`${item.image.asset.url}?w=500&h=400&fit=crop`}
              alt={item.name}
              style={{
                background: `url(${item.image.asset.metadata.lqip})`,
                backgroundSize: 'cover',
              }}
            />
            <div className="title">{item.name}</div>
          </ItemStyles>
        </a>
      ))}
    </ItemsGrid>
  );
}
