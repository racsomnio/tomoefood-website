import React from 'react';
import { Link } from 'gatsby';
import Img from 'gatsby-image';
import styled from 'styled-components';

const ProductGridStyle = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
  grid-gap: 4rem;
  /* grid-auto-rows: 300px auto; */
`;

const ProductStyles = styled.div`
  display: grid;
  border-radius: 15px;
  margin: 2rem;
  border: 1px solid var(--grey);
  padding: 2rem;
  text-align: center;

  @supports not (grid-template-rows: subgrid) {
    --rows: auto auto 1fr;
  }
  /* take sizing from the parent only if FF */
  grid-template-rows: var(--rows, subgrid);
  grid-row: span 3;
  grid-gap: 1rem;
  h2,
  p {
    margin: 2rem 0 0;
  }
`;

function SingleProduct({ product }) {
  return (
    <ProductStyles>
      <Link to={`/products/${product.slug.current}`}>
        {/* <p>{product.origin.map((origin) => origin.name).join(', ')}</p> */}
        <Img fluid={product.image.asset.fluid} alt={product.name} />
        <h2>
          <span className="mark">{product.name}</span>
        </h2>
      </Link>
    </ProductStyles>
  );
}

function ProductList({ products }) {
  return (
    <ProductGridStyle>
      {products.map((product) => (
        <SingleProduct key={product.id} product={product} />
      ))}
    </ProductGridStyle>
  );
}

export default ProductList;
