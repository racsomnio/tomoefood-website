import React, { useState } from 'react';
import { Link } from 'gatsby';
import Headroom from 'react-headroom';
import styled from 'styled-components';
import logo from '../assets/images/logo.png';

const NavStyles = styled.nav`
  margin-bottom: 5rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;
  padding: 2rem;

  .headroom--unpinned &,
  .headroom--pinned & {
    background: rgba(0, 0, 0, 0.95);
    border-bottom: 1px solid var(--secondary);
  }

  .logo {
    line-height: 0;
    flex: 0 0 100px;

    .headroom--unpinned &,
    .headroom--pinned & {
      flex: 0 0 50px;
    }
  }

  ul {
    margin: 0;
    padding: 0;
    list-style: none;
    position: absolute;
    display: none;
    flex-direction: column;
    left: 100%;
    top: 85px;
    padding: 2rem;
    gap: 2rem;
    z-index: 11;
    border-left: 1px solid var(--secondary);

    @media (min-width: 780px) {
      display: flex;
      position: relative;
      flex-direction: row;
      top: initial;
      left: initial;
      border-left: initial;
    }

    &.open {
      display: flex;
    }
  }

  li {
    margin-bottom: 2rem;
    transition: background 0.5s;
    @media (min-width: 780px) {
      margin-bottom: 0;
    }
    &:hover {
      background: #fff;
    }
  }
  a {
    font-size: 3rem;
    text-decoration: none;
    display: block;
    padding: 0.5rem 1rem;
    color: #fff;

    @media (min-width: 780px) {
      font-size: 2rem;
      color: inherit;
    }

    &:hover {
      color: #000;
    }

    &[aria-current='page'] {
      background: #000;
      color: var(--secondary);
    }
  }
`;

const HamburgerIcon = styled.span`
  cursor: pointer;
  display: grid;
  flex-direction: column;
  gap: 5px;

  @media (min-width: 780px) {
    display: none;
  }

  .bar {
    background: var(--secondary);
    height: 5px;
    width: 40px;
  }

  .layover {
    position: fixed;
    left: 0;
    top: 0;
    width: 100vw;
    height: 100vh;
    display: ${({ open }) => (open ? 'block' : 'none')};
    z-index: 10;
  }
`;

export default function Nav({ location }) {
  const [open, toggle] = useState(false);

  function toggleHamburgerClick() {
    toggle(!open);
    if (!open) {
      document.body.classList.add('nav-open');
    } else {
      document.body.classList.remove('nav-open');
    }
  }

  return (
    <Headroom disableInlineStyles style={{ height: '120px' }}>
      <NavStyles location={location}>
        <Link to="/" className="logo">
          <img src={logo} alt="Tomoe Food Services" />
        </Link>

        <HamburgerIcon
          onClick={toggleHamburgerClick}
          onKeyDown={toggleHamburgerClick}
          data-target="navMenu"
          aria-label="Open the menu"
          aria-hidden="true"
          open={open}
        >
          <span className="bar" />
          <span className="bar" />
          <span className="bar" />
          <div className="layover" />
        </HamburgerIcon>

        <ul
          role="presentation"
          className={open ? 'open' : ''}
          onClick={toggleHamburgerClick}
        >
          <li>
            <Link to="/about">About</Link>
          </li>
          <li>
            <a href="TomoeItemList.pdf" target="_blank">
              Catalog
            </a>
          </li>
          <li>
            <Link to="/new-customer">New Customer</Link>
          </li>
          <li>
            <Link to="/location">Location</Link>
          </li>
          <li>
            <Link to="/contact">Contact</Link>
          </li>
        </ul>
      </NavStyles>
    </Headroom>
  );
}
