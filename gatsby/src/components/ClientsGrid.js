import React from 'react';
import styled from 'styled-components';

const ClientStyles = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background: #fff;

  span {
    padding: 2rem;
  }
`;

export default function ItemGrid({ items }) {
  return (
    <ClientStyles>
      {items.map((item) => (
        <span key={item._id}>
          <a href={item.url} target="_blank" rel="noreferrer">
            <img
              width="100"
              src={`${item.image.asset.url}?w=130`}
              alt={item.name}
              style={{
                background: `url(${item.image.asset.metadata.lqip})`,
                backgroundSize: 'cover',
                boxShadow: '0 0 0 10px #fff',
              }}
            />
          </a>
        </span>
      ))}
    </ClientStyles>
  );
}
