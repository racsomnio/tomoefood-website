import React from 'react';
import styled from 'styled-components';
import { Link } from 'gatsby';
import { MdLocationOn } from 'react-icons/md';
import { IoMdClock } from 'react-icons/io';
import { FaFacebook, FaInstagram, FaTwitter } from 'react-icons/fa';
import bg from '../assets/images/j-pattern.jpg';

const FooterStyles = styled.footer`
  padding: 2rem;
  margin-top: 5rem;
  background: ${({ bg }) => bg && `url(${bg})`};
  color: #fff;

  a {
    color: #fff;
    padding: 1rem;
  }

  .foot-body {
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
    max-width: 650px;
    margin: auto;
  }

  .foot-nav {
    display: flex;
    flex-wrap: wrap;
    align-items: center;

    a {
      border: 1px solid #fff;
      margin: 2rem;
    }
  }

  .foot-geo-section {
    padding: 2rem;
  }

  .center {
    margin-top: 4rem;
  }
`;

export default function Footer() {
  return (
    <FooterStyles bg={bg}>
      <div className="foot-body">
        <div className="foot-nav">
          <Link to="/about">About</Link>
          <a href="/TomoeItemList.pdf" target="_blank">
            Catalog
          </a>
          <Link to="/location">Location</Link>
          <Link to="/new-customer">New Customer</Link>
          <Link to="/contact">Contact</Link>
        </div>
        <div className="foot-geo">
          <div className="foot-geo-section">
            <MdLocationOn />
            <br />
            355 Food Center Drive, <br />
            Building E 15 Room-105
            <br />
            Bronx, NY 10474
            <br />
            Hunts Point Cooperative Market
          </div>
          <div className="foot-geo-section">
            <IoMdClock />
            <br />
            Opening Hours
            <br />
            Monday to Friday: 7am-5pm
            <br />
            Sat & Sun: Close
          </div>
          <div>
            <a
              href="https://www.facebook.com/Tomoe-Food-Services-101635631782428"
              className="foot-geo-section"
              target="_blank"
              rel="noreferrer"
            >
              <FaFacebook />
            </a>
            <a
              href="https://twitter.com/TomoeFoodNY"
              className="foot-geo-section"
              target="_blank"
              rel="noreferrer"
            >
              <FaTwitter />
            </a>
            <a
              href="https://www.instagram.com/tomoe.food/"
              className="foot-geo-section"
              target="_blank"
              rel="noreferrer"
            >
              <FaInstagram />
            </a>
          </div>
        </div>
      </div>
      <div className="center">
        &copy; Tomoe Food Services Inc. {new Date().getFullYear()}
      </div>
    </FooterStyles>
  );
}
