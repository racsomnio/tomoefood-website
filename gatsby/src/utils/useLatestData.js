import { useState, useEffect } from 'react';

const gql = String.raw;
const imageDeets = `
  image {
    asset {
      url
      metadata {
        lqip
      }
    }
  }
`;

export default function useLatestData() {
  // clients
  const [newClients, setNewClients] = useState();
  // restaurants
  const [newRestaurants, setNewRestaurants] = useState();
  // products
  const [popularProducts, setPopularProducts] = useState();

  useEffect(function () {
    // when the component loads, fetch the data
    fetch(process.env.GATSBY_GRAPHQL_ENDPOINT, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        query: gql`
          query {
            StoreSettings(id: "stock") {
              name
              clients {
                name
                url
                _id
                ${imageDeets}
              }
              restaurants {
                name
                url
                _id
                ${imageDeets}
              }
              popularProducts {
                name
                _id
                url
                ${imageDeets}
              }
            }
          }
        `,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        // TODO: checl for errors
        // set the data to state
        setNewClients(res.data.StoreSettings.clients);
        setNewRestaurants(res.data.StoreSettings.restaurants);
        setPopularProducts(res.data.StoreSettings.popularProducts);
      })
      .catch((err) => {
        console.log('SHOOOOOT');
        console.log(err);
      });
  }, []);

  return {
    newClients,
    newRestaurants,
    popularProducts,
  };
}
