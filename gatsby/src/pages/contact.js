import React from 'react';
import styled from 'styled-components';
import { FaPhoneAlt, FaFax } from 'react-icons/fa';
import { GrMail } from 'react-icons/gr';

const ContactStyles = styled.div`
  margin: auto;
  padding: 0 2rem;
  max-width: 1024px;

  h1,
  h3 {
    margin-bottom: 2rem;
  }

  svg {
    vertical-align: middle;
    margin-right: 2rem;
  }

  .columns {
    display: grid;
    gap: 4rem;
    justify-content: center;
    align-items: flex-start;

    @media (min-width: 1024px) {
      grid-template-columns: 320px 1fr;
      gap: 8rem;
    }
  }

  .info {
    /* flex: 0 1 320px; */
    background: var(--secondary);
    color: #000;
    padding: 2rem;
    font-weight: bold;

    a {
      font-weight: bold;
      color: #000;
    }
  }

  input,
  select,
  textarea {
    border: 1px solid #bbb;
    border-radius: 3px;
    display: block;
    width: 100%;
    padding: 1rem;
    box-sizing: border-box;
    box-shadow: 0px 7px 10px -5px rgba(0, 0, 0, 0.1);
  }

  form {
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(280px, 1fr));
    grid-gap: 3rem;
  }
  .full-width {
    grid-column: 1 / -1;
  }
`;

const BgImage = styled.div`
  background: url('/contact.jpg') no-repeat;
  background-size: 100%;
  background-position: center;
  width: 100%;
  height: 200px;
`;

function ContactPage() {
  return (
    <ContactStyles>
      <h1 className="center">Get in Touch</h1>
      <div className="columns">
        <div className="info">
          <BgImage />
          <p>
            We are Japanese meat distributor located in New York City. We
            deliver to restaurants and grocery stores in NY and around NY. We
            carry beef, pork, poultry and more. ​ Email: tomoefood@gmail.com
          </p>
          <p>
            <a href="tel:+17183281129">
              <FaPhoneAlt /> Tel: (718) 328-1129
            </a>
          </p>
          <p>
            <FaFax /> Fax: (718) 328-1130
          </p>
          <p>
            <GrMail />
            tomoefood@gmail.com
          </p>
        </div>
        <div>
          <h3>
            Please fill out the form and we will get back to you as soon as
            possible.
          </h3>
          <form
            method="post"
            netlify-honeypot="bot-field"
            data-netlify="true"
            name="contact v2"
          >
            <input type="hidden" name="bot-field" />
            <input type="hidden" name="form-name" value="contact v2" />
            <p className="full-width">
              <label htmlFor="subject">
                Meat of the matter
                <select name="subject[]" id="subject">
                  <option value="">Please select one</option>
                  <option value="General">General</option>
                  <option value="Sales">Sales</option>
                  <option value="Billing">Billing</option>
                  <option value="Shipping">Shipping</option>
                  <option value="Technical">Technical</option>
                  <option value="Credit">Credit</option>
                </select>
              </label>
            </p>
            <p>
              <label htmlFor="name">
                Name
                <input type="text" name="name" id="name" required />
              </label>
            </p>
            <p>
              <label htmlFor="lastname">
                Las Name
                <input type="text" name="lastname" id="lastname" required />
              </label>
            </p>
            <p className="full-width">
              <label htmlFor="email">
                Email
                <input type="email" name="email" id="email" required />
              </label>
            </p>
            <p>
              <label htmlFor="address1">
                Address 1
                <textarea name="address1" id="address1" rows="2" />
              </label>
            </p>
            <p>
              <label htmlFor="address2">
                Address 2
                <textarea name="address2" id="address2" rows="2" />
              </label>
            </p>
            <p>
              <label htmlFor="city">
                City
                <input type="text" name="city" id="city" required />
              </label>
            </p>
            <p>
              <label htmlFor="state">
                State
                <input type="text" name="state" id="state" required />
              </label>
            </p>
            <p>
              <label htmlFor="zipcode">
                Zip Code
                <input type="text" name="zipcode" id="zipcode" />
              </label>
            </p>
            <p>
              <label htmlFor="phone">
                Phone
                <input type="text" name="phone" id="phone" />
              </label>
            </p>
            <p>
              <label htmlFor="fax">
                Fax
                <input type="text" name="fax" id="fax" />
              </label>
            </p>
            <p className="full-width">
              <label htmlFor="message">
                Message
                <textarea name="message" id="message" rows="5" />
              </label>
            </p>

            <button type="submit">Send</button>
          </form>
        </div>
      </div>
    </ContactStyles>
  );
}

export default ContactPage;
