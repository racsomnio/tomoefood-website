import React from 'react';
import styled from 'styled-components';
import { LocalBusinessJsonLd } from 'gatsby-plugin-next-seo';

const LocationStyles = styled.div`
  max-width: 600px;
  margin: auto;

  h1 {
    padding: 0 2rem 2rem;
  }

  .card {
    display: flex;
    flex-direction: column;

    @media (min-width: 500px) {
      flex-direction: row;
    }
  }
  .location-text {
    background: #000;
    color: #fff;
    padding: 2rem;
  }
  .extra-info {
    padding: 2rem;
  }
`;

export default function location() {
  return (
    <>
      <LocalBusinessJsonLd
        type="LocalBusiness"
        id="https://tomoefoodservices.com"
        name="Tomoe Food Services"
        description="We supply hight-quality meat to the best restaurants in NYC. Best priced Japanese wagyu beef in New York. Tomoe Food Services also sells the highest quality American Kobe Beef, Canadian Pork, US beef, poultry and duck."
        url="http://tomoefoodservices.com/location"
        telephone="+17183281129"
        address={{
          streetAddress: '355 Food Center Dr, Building E15, Room 105',
          addressLocality: 'The Bronx',
          addressRegion: 'NY',
          postalCode: '10474',
          addressCountry: 'US',
        }}
        geo={{
          latitude: '40.806823',
          longitude: '-73.873669',
        }}
        images={[
          'https://tomoefoodservices.com/meat.jpg',
          'https://tomoefoodservices.com/tomoe-at-hunters-point.jpg',
        ]}
      />

      <LocationStyles>
        <h1 className="center seccondary-bg">We are New Yorkers</h1>
        <a
          href="https://www.google.com/maps/place/Hunts+Point+Cooperative+Market/@40.8066708,-73.8803029,15z/data=!4m12!1m6!3m5!1s0x89c2f510a0b8c4e5:0x28c7a28f35e1a4f6!2sHunts+Point+Cooperative+Market!8m2!3d40.8067972!4d-73.873673!3m4!1s0x89c2f510a0b8c4e5:0x28c7a28f35e1a4f6!8m2!3d40.8067972!4d-73.873673"
          target="_blank"
          rel="noreferrer"
        >
          <div className="card">
            <div className="location-text">
              <h2>Tomoe Food Services</h2>
              <div>
                <p>355 Food Center Drive, Building E 15 Room-105</p>
                <p>Bronx, NY 10474</p>
                <p>Hunts Point Cooperative Market</p>
              </div>
            </div>
            <img src="/map-large.jpg" alt="tomoe food location map" />
          </div>
        </a>
        <div className="extra-info center">
          <h3>Feel free to walk-in</h3>
          <p>
            Or take a look on our online shop
            <br />
            <a className="primary-bg" href="https://orderwagyu.com">
              here
            </a>
          </p>
        </div>
      </LocationStyles>
    </>
  );
}
