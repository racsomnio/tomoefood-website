import React from 'react';
import styled from 'styled-components';

const AboutStyles = styled.div`
  --columns: 1;
  column-count: var(--columns);
  column-gap: 10rem;
  text-align: left;
  line-height: 2.5rem;
  padding: 2rem;

  @media (min-width: 600px) {
    --columns: 2;
  }

  img {
    margin: 2rem auto 4rem;
  }
`;

function AboutUsPage() {
  return (
    <>
      <AboutStyles className="content">
        <h1>About Tomoe</h1>
        <p>
          Tomoe Food Services, Inc. was established in 2014 as a distributor of
          wholesale meat distributor and is located in the Hunts Point
          Cooperative Market in the Bronx, New York.
          <img
            src="/tomoe-at-hunters-point.jpg"
            alt="Tomoe Food is in Hunters Point NY"
          />
        </p>
        <p>
          Tomoe Food has supplied meat to distributors, restaurants, supermarket
          and this endeavor continues to be our main focus.
        </p>
        <p>
          We work with large farms from all around the country to seek the best
          meats from the best growers. We deliver 5 days a week to NYC.
        </p>
        <p>
          We distribute only the finest wholesale meats to many of the caters,
          restaurants, distributors and supermarkets. Tomoe Food is the largest
          wholesale distributor of Japanese wagyu beef on the East Coast. We are
          sure you'll enjoy the unique flavor.
        </p>
        <p>
          Tomoe Food sells the highest quality American wagyu beef, Berkshire
          pork, prime beef, organic chicken and duck.
        </p>
        <h3>SERVICES</h3>
        <p>
          Our reputation allows us to take advantage of volume and program
          buying. We can provide our customers with a vast array of top-quality
          meats, custom-cut meats as well as the quality service, and
          competitive pricing that our long list of satisfied customers have
          come to expect.
          <img src="/meat.jpg" alt="Tomoe food has the best cuts" />
        </p>
      </AboutStyles>
    </>
  );
}

export default AboutUsPage;
