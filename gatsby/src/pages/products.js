import React from 'react';
import { graphql } from 'gatsby';
import ProductList from '../components/ProductList';
import TagsFilter from '../components/TagsFilter';
import SEO from '../components/SEO';

export default function ProductsPage({ data, pageContext }) {
  const products = data.products.nodes;
  return (
    <>
      <SEO title={pageContext.tag ? pageContext.tag : 'All Products'} />
      <TagsFilter />
      <ProductList products={products} />
    </>
  );
}

export const query = graphql`
  query ProductsQuery($tagRegex: String) {
    products: allSanityProducts(
      filter: { origin: { elemMatch: { name: { regex: $tagRegex } } } }
    ) {
      nodes {
        name
        image {
          asset {
            fluid(maxWidth: 400) {
              ...GatsbySanityImageFluid
            }
            fixed(width: 200, height: 200) {
              ...GatsbySanityImageFixed
            }
          }
        }
        id
        origin {
          name
          id
          category
        }
        price
        slug {
          current
        }
      }
    }
  }
`;
