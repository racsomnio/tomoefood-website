import React from 'react';

function FourZeroFourPage() {
  return <div>Oops! That page doesn't exist!!!</div>;
}

export default FourZeroFourPage;
