import React from 'react';
import { graphql } from 'gatsby';
import Img from 'gatsby-image';
import styled from 'styled-components';
import SEO from '../components/SEO';

const ClientsGrid = styled.div`
  display: grid;
  grid-gap: 2rem;
  grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
`;

const ClientStyles = styled.div`
  a {
    text-decoration: none;
  }

  .gatsby-image-wrapper {
    height: 300px;
    /* & img {
      object-fit: contain;
    } */
  }
`;

export default function ClientsPage({ data }) {
  const clients = data.clients.nodes;
  return (
    <>
      <SEO title="Clients" />
      <p>{process.env.GATSBY_PAGE_SIZE}--</p>
      <ClientsGrid>
        {clients.map((client) => (
          <ClientStyles>
            <a href={client.url} target="_blank" rel="noreferrer">
              <Img fluid={client.image.asset.fluid} />
              <h2>
                <span className="mark">{client.name}</span>
              </h2>
            </a>
            <p className="description">{client.description}</p>
          </ClientStyles>
        ))}
      </ClientsGrid>
    </>
  );
}

export const query = graphql`
  query MyQuery {
    clients: allSanityClients {
      totalCount
      nodes {
        id
        name
        url
        description
        image {
          asset {
            fluid(maxWidth: 200) {
              ...GatsbySanityImageFluid
            }
          }
        }
      }
    }
  }
`;
