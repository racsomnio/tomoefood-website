import React from 'react';
import { GatsbySeo } from 'gatsby-plugin-next-seo';
import styled from 'styled-components';
import useLatestData from '../utils/useLatestData';
import { HomePageGrid } from '../styles/Grids';
import LoadingGrid from '../components/LoadingGrid';
import ItemGrid from '../components/ItemGrid';
import ClienstGrid from '../components/ClientsGrid';

const JumbotronStyles = styled.div`
  -webkit-font-smoothing: antialiased;
  background: url('/jumbotron.jpg') no-repeat;
  background-position: center 120px;
  color: #fff;
  height: 103vh;
  max-height: 760px;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  margin-top: -10rem;
  padding-bottom: 9rem;
  box-sizing: border-box;
  @media (min-width: 1040px) {
    background: url('/jumbotron-large.jpg') no-repeat;
    background-attachment: fixed;
    justify-content: space-around;
    align-items: baseline;
    background-position: calc(50% + 200px) calc(50% + 50px);
  }

  h1 {
    padding: 0 1rem;
    max-width: 80%;
    /* background: rgba(0, 0, 0, 0.5); */
    text-shadow: -2px 4px 8px rgba(0, 0, 0, 0.6),
      -1px 3px 1px rgba(0, 0, 0, 0.6);
    font-size: 3rem;
    text-transform: uppercase;

    @media (min-width: 768px) {
      font-size: 4rem;
    }
    @media (min-width: 1040px) {
      width: 300px;
      left: calc(50% - 450px);
      position: relative;
      text-align: right;
    }
  }
  img {
    margin-bottom: 1rem;
  }
`;

const LocationBannerStyles = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
  margin-bottom: 6rem;
  align-items: center;

  .info {
    padding: 1rem 2rem;
    text-align: left;
  }
`;

const NewsStyles = styled.div`
  background: #000;
  display: flex;
  color: var(--secondary);
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 2rem;
  margin-top: 6rem;
  border-top: 1px double var(--secondary);
  border-bottom: 1px double var(--secondary);

  a {
    color: #fff;
  }
  img {
    margin: 1rem;
  }
`;

const TwoCols = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
  gap: 8rem;
`;

function NewClients({ newClients }) {
  return (
    <div>
      <h2 className="center secondary-bg">Our Clients</h2>
      {!newClients && <LoadingGrid count={2} />}
      {newClients && !newClients?.length && <p>Join out network</p>}
      {newClients?.length && <ClienstGrid items={newClients} />}
    </div>
  );
}

function NewRestaurants({ newRestaurants }) {
  return (
    <div>
      <h2 className="center primary-bg">Our Restaurants</h2>
      {!newRestaurants && <LoadingGrid count={2} />}
      {newRestaurants && !newRestaurants?.length && <p>Join out network</p>}
      {newRestaurants?.length && <ClienstGrid items={newRestaurants} />}
    </div>
  );
}

function PopularProducts({ popularProducts }) {
  return (
    <div>
      <div>
        <h2 className="primary-bg">Shop Online</h2>
        {!popularProducts && <LoadingGrid count={2} />}
        {popularProducts && !popularProducts?.length && <p>Coming soon</p>}
        {popularProducts?.length && <ItemGrid items={popularProducts} />}
      </div>
    </div>
  );
}

function HomePage() {
  const { newClients, popularProducts, newRestaurants } = useLatestData();

  return (
    <>
      <GatsbySeo
        title="Best wagyu distributor in NYC"
        description="Tomoe Food supplies hight-quality meat to the best restaurants in NYC. Best priced Japanese wagyu beef in New York."
        image="/best-wagyu-distributor-nyc.jpg"
        openGraph={{
          images: [{ url: '/best-wagyu-distributor-nyc.jpg' }],
        }}
      />
      <div className="center">
        <JumbotronStyles>
          <h1>Trusted by the best restaurants in NYC</h1>
        </JumbotronStyles>

        <HomePageGrid>
          <LocationBannerStyles>
            <div className="info">
              <h2>We specialize in high-quality meat.</h2>
              <p>
                Tomoe Food has been distributing the highest quality of beef
                since 2014 to the best restaurants in NYC.
              </p>
              <p>
                We sell the highest quality Kobe style meat, 100% Berkshire
                pork, Dubreton pork and Mugifuji pork from Canada.
              </p>
              <p>
                Tomoe Food is located in the Hunts Point Cooperative Market in
                the Bronx, New York.
              </p>
            </div>
            <img
              src="/food-sample.jpg"
              alt="tomoe food meat for best restaurants"
            />
          </LocationBannerStyles>

          <PopularProducts popularProducts={popularProducts} />

          <TwoCols>
            <NewClients newClients={newClients} />

            <NewRestaurants newRestaurants={newRestaurants} />
          </TwoCols>

          <NewsStyles>
            <div>
              <h2>Featured In</h2>
              <a
                href="https://www.bloomberg.com/news/articles/2020-10-01/ozaki-beef-is-the-even-more-premium-upgrade-on-japanese-wagyu"
                target="_blank"
                rel="noreferrer"
              >
                <img
                  src="/logo-bloomberg.png"
                  alt="tomoe food article in bloomberg"
                  width="140px"
                />
              </a>
              <a
                href="https://www.nytimes.com/2020/11/10/dining/nyc-restaurant-news.html#link-3b1e5bd7"
                target="_blank"
                rel="noreferrer"
              >
                <img
                  src="/new-york-times.png"
                  alt="tomoe food article in bloomberg"
                />
              </a>
            </div>
          </NewsStyles>
        </HomePageGrid>
      </div>
    </>
  );
}

export default HomePage;
