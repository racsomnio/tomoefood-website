import React from 'react';
import styled from 'styled-components';
import { MdAssignment } from 'react-icons/md';
import { RiShieldCheckFill } from 'react-icons/ri';

const CustomerStyles = styled.div`
  max-width: 500px;
  margin: auto;
  padding: 0 2rem;

  .section {
    display: flex;
    align-items: center;
    margin: 4rem 0;
  }

  .circle {
    background: var(--secondary);
    border-radius: 50%;
    margin-right: 1rem;
    padding: 1.5rem;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 3rem;
    color: #000;
  }
`;

function NewCustomerPage() {
  return (
    <CustomerStyles>
      <h1>If you are new customer</h1>
      <p>Please fill up the following:</p>
      <a href="/credit-application.pdf" target="_blank" rel="noreferrer">
        <p className="section">
          <span className="circle">
            <MdAssignment />
          </span>
          <span>Credit Application</span>
        </p>
      </a>
      <a href="/resale-certificate.pdf" target="_blank" rel="noreferrer">
        <p className="section">
          <span className="circle">
            <RiShieldCheckFill />
          </span>
          <span>Resale Certification</span>
        </p>
      </a>
    </CustomerStyles>
  );
}

export default NewCustomerPage;
