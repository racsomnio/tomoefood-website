import path, { resolve } from 'path';
// import fetch from 'isomorphic-fetch';

async function turnProductsIntoPages({ graphql, actions }) {
  // 1. Get a template for this page
  const productTemplate = path.resolve('./src/templates/Product.js');
  // 2. Query all products
  const { data } = await graphql(`
    query {
      products: allSanityProducts {
        nodes {
          name
          slug {
            current
          }
        }
      }
    }
  `);
  // 3. Loop over each product and create a page for that product
  data.products.nodes.forEach((product) => {
    actions.createPage({
      // What is the URL for this new page??
      path: `products/${product.slug.current}`,
      component: productTemplate,
      context: {
        slug: product.slug.current,
      },
    });
  });
}

async function turnTagsIntoPages({ graphql, actions }) {
  // 1. Get the template
  const tagTemplate = path.resolve('./src/pages/products.js');
  // 2. query all the tags
  const { data } = await graphql(`
    query {
      tags: allSanityOrigin {
        nodes {
          name
          id
        }
      }
    }
  `);
  // 3. createPage for that topping
  data.tags.nodes.forEach((tag) => {
    actions.createPage({
      path: `products/${tag.name}`,
      component: tagTemplate,
      context: {
        tag: tag.name,
        tagRegex: `/${tag.name}/i`,
      },
    });
  });
}

// async function fetchBeersAndTurnIntoNodes({
//   actions,
//   createNodeId,
//   createContentDigest,
// }) {
//   // 1. Fetch a  list of beers
//   const res = await fetch('https://sampleapis.com/beers/api/ale');
//   const beers = await res.json();
//   // 2. Loop over each one
//   for (const beer of beers) {
//     // create a node for each beer
//     const nodeMeta = {
//       id: createNodeId(`beer-${beer.name}`),
//       parent: null,
//       children: [],
//       internal: {
//         type: 'Beer',
//         mediaType: 'application/json',
//         contentDigest: createContentDigest(beer),
//       },
//     };
//     // 3. Create a node for that beer
//     actions.createNode({
//       ...beer,
//       ...nodeMeta,
//     });
//   }
// }

// async function turnSlicemastersIntoPages({ graphql, actions }) {
//   // 1. Query all slicemasters
//   const { data } = await graphql(`
//     query {
//       slicemasters: allSanityPerson {
//         totalCount
//         nodes {
//           name
//           id
//           slug {
//             current
//           }
//         }
//       }
//     }
//   `);
//   // TODO: 2. Turn each slicemaster into their own page (TODO)
//   data.slicemasters.nodes.forEach((slicemaster) => {
//     actions.createPage({
//       component: resolve('./src/templates/Slicemaster.js'),
//       path: `/slicemaster/${slicemaster.slug.current}`,
//       context: {
//         name: slicemaster.person,
//         slug: slicemaster.slug.current,
//       },
//     });
//   });

//   // 3. Figure out how many pages there are based on how many slicemasters there are, and how many per page!
//   const pageSize = parseInt(process.env.GATSBY_PAGE_SIZE);
//   const pageCount = Math.ceil(data.slicemasters.totalCount / pageSize);
//   console.log(
//     `There are ${data.slicemasters.totalCount} total people. And we have ${pageCount} pages with ${pageSize} per page`
//   );
//   // 4. Loop from 1 to n and create the pages for them
//   Array.from({ length: pageCount }).forEach((_, i) => {
//     console.log(`Creating page ${i}`);
//     actions.createPage({
//       path: `/slicemasters/${i + 1}`,
//       component: path.resolve('./src/pages/slicemasters.js'),
//       // This data is pass to the template when we create it
//       context: {
//         skip: i * pageSize,
//         currentPage: i + 1,
//         pageSize,
//       },
//     });
//   });
// }

// export async function sourceNodes(params) {
//   // fetch a list of beers and source them into our gatsby API!
//   await Promise.all([fetchBeersAndTurnIntoNodes(params)]);
// }

export async function createPages(params) {
  // Create pages dynamically
  // Wait for all promises to be resolved before finishing this function
  await Promise.all([
    turnProductsIntoPages(params),
    turnTagsIntoPages(params),
    // turnSlicemastersIntoPages(params),
  ]);
  // 1. Products
  // 2. Tags
}
