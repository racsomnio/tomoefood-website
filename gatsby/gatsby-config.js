import dotenv from 'dotenv';

dotenv.config({ path: '.env' });

export default {
  siteMetadata: {
    title: 'Tomoe Food',
    siteUrl: 'https://tomoefoodservices.com',
    description: 'We deliver the best meat from Japan in NYC and surroundings.',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-plugin-next-seo',
      options: {
        titleTemplate: '%s | Tomoe Food Services',
        openGraph: {
          type: 'website',
          locale: 'en_US',
          url: 'https://tomoefoodservices.com',
          site_name: 'SiteName',
        },
      },
    },
    'gatsby-plugin-styled-components',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Tomoe Food Services`,
        short_name: `Tomoe Food`,
        start_url: `/`,
        background_color: `#000`,
        theme_color: `#000`,
        display: `standalone`,
        icon: `src/assets/images/logo.png`,
      },
    },
    {
      // this is the name of the plugin you are adding
      resolve: 'gatsby-source-sanity',
      options: {
        projectId: 'wilb8sk5',
        dataset: 'production',
        watchMode: true,
        token: process.env.SANITY_TOKEN,
      },
    },
  ],
};
